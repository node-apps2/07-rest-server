const mongoose = require('mongoose')

let Schema = mongoose.Schema

let CategoriaSchema = new Schema({
    nombre: {
        type: String,
        unique: true,
        required: [true, 'Nombre de la categoria requerida']
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario'
        // required: [true, 'id de usuario no especificado']
    }
})

module.exports = mongoose.model('Categoria', CategoriaSchema);