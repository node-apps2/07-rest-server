const jwt = require('jsonwebtoken');

let verifyToken = (req, res, next) => {
    let token = req.get('Authorization');
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: 'token no reconocido'
            })
        }
        req.usuario = decoded.usuario;
        next();

    })
}

let verifyAdminRol = (req, res, next) => {
    let usuario = req.usuario

    if (usuario.role !== 'ADMIN_ROLE') {
        return res.status(401).json({
            ok: false,
            err: 'No tienes autorizacion de rol'
        })
    } else {

        next();
    }
}

let verifyTokenUrl = (req, res, next) => {
    let token = req.query.Authorization;

    // res.json({ token })
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: 'token no reconocido'
            })
        }
        req.usuario = decoded.usuario;
        next();

    })
}

module.exports = { verifyToken, verifyAdminRol, verifyTokenUrl }