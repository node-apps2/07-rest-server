//==========================
//puerto
//==========================
process.env.PORT = process.env.PORT || 3000;

//==========================
//entorno
//==========================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
//==========================
//vencimiento del token 1h
//==========================
process.env.CADUCIDAD_TOKEN = '24h'

//==========================
//seed de autenticacion
//==========================
process.env.SEED = process.env.SEED || 'seed_autenticacion_desarrollo'

//==========================
// GOOGLE CLIENT_ID
//==========================
process.env.CLIENT_ID = process.env.CLIENT_ID || '728231954388-ncpaua33ql3kckfg5d9l6o39erqcjd4r.apps.googleusercontent.com'

//==========================
//db
//==========================

let urlDB = (process.env.NODE_ENV === 'dev') ? 'mongodb://localhost:27017/cafe' : process.env.MONGO_URI

process.env.URLDB = urlDB
