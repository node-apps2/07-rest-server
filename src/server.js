require('./config/config')

const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const path = require('path')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//habilitar el public
app.use(express.static(path.resolve(__dirname + '/../public')));


// console.log(path.resolve(__dirname, '../public'));

app.use(require('./routes/routes'))



mongoose.connect(process.env.URLDB, {
    useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false
}, (err, res) => {
    if (err) throw err;
    console.log('base de datos online');

});

app.listen(process.env.PORT, () => {
    console.log(`servidor escuchando en el puerto ${process.env.PORT}`);
})



