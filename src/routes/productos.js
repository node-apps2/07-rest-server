const express = require('express')
const app = express();

const { verifyToken } = require('../middlewares/autenticacion')

const Producto = require('../models/producto')


//buscar productos

app.get('/producto/buscar/:termino', verifyToken, (req, res) => {
    let termino = req.params.termino
    let regex=new RegExp(termino,'i');
    Producto.find({ nombre: regex })
        .populate('categoria', 'nombre')
        .exec((err, productoDB) => {
            if (err) {
                return res.status(500).json({ ok: false, err })
            }

            res.json({ ok: true, productos: productoDB })
        })
})

//guardar productos
app.post('/producto', verifyToken, (req, res) => {

    let body = req.body

    let producto = new Producto({
        nombre: body.nombre,
        precioUni: body.precioUni,
        descripcion: body.descripcion,
        disponible: body.disponible,
        categoria: body.categoria,
        usuario: req.usuario._id
    })

    producto.save((err, productoDB) => {
        if (err) {
            return res.status(500).json({ ok: false, err })
        }

        if (!productoDB) {
            return res.status(400).json({ ok: false, err })
        }

        res.json({ ok: true, producto: productoDB })
    })
})

//Obtener productos de manera paginada

app.get('/producto', verifyToken, (req, res) => {
    let desde = req.query.desde || 0
    let limite = req.query.limite || 5

    desde = Number(desde)
    limite = Number(limite)

    Producto.find({})
        .skip(desde)
        .limit(limite)
        .populate('categoria', 'nombre')
        .populate('usuario', 'nombre email')
        .exec((err, productoDB) => {
            if (err) {
                return res.status(400).json({ ok: false, err })
            }
            res.json({ ok: true, productos: productoDB })
        })


})

//obetener un producto por id
app.get('/producto/:id', verifyToken, (req, res) => {
    let id = req.params.id

    Producto.findById(id, (err, productoDB) => {
        if (err) {
            return res.status(500).json({ ok: false, err })
        }
        if (!productoDB) {
            return res.status(400).json({ ok: false, err: { message: 'Producto no encontrado' } })
        }
        res.json({ ok: true, producto: productoDB })
    })
})

//actualizar un producto
app.put('/producto/:id', verifyToken, (req, res) => {
    let id = req.params.id
    let body = req.body

    let productoNuevo = {
        nombre: body.nombre,
        precioUni: body.precioUni,
        descripcion: body.descripcion,
        disponible: body.disponible,
        categoria: body.categoria,
        usuario: req.usuario._id
    }

    Producto.findOneAndUpdate(id, productoNuevo, { new: true, runValidators: true }, (err, productoDB) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if (!productoDB) {
            return res.status(400).json({ ok: false, message: 'Producto no encontrado' })
        }

        res.json({ ok: true, producto: productoDB });
    })
})

app.delete('/producto/:id', verifyToken, (req, res) => {

    let id = req.params.id

    Producto.findByIdAndUpdate(id, { disponible: false }, (err, productoDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }
        if (!productoDB) {
            res.status(400).json({ ok: false, err: { message: 'producto no encontrado' } })

        }

        res.json({ ok: true, message: 'Producto ya no disponible' })
    })
})




module.exports = app