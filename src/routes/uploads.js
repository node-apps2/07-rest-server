const express = require('express');
const path = require('path')
const fileUpload = require('express-fileupload');
const app = express();
const Usuario = require('../models/usuario')
const Producto = require('../models/producto')
const fs = require('fs')
app.use(fileUpload({ useTempFiles: true }));


app.put('/upload/:tipo/:id', function (req, res) {

    let tipo = req.params.tipo
    let id = req.params.id

    if (!req.files || Object.keys(req.files).length === 0) {
        res.status(400).json({ ok: false, message: 'Ningun archivo ha sido seleccionado' });
        return;
    }
    //validar tipos
    let tiposValidos = ['producto', 'usuario'];
    if (tiposValidos.indexOf(tipo) < 0) {
        return res.status(400).json({ ok: false, err: { message: `Los tipos permitidos son ${tiposValidos.join(', ')}` } })
    }


    let archivo = req.files.archivo;

    let nombreCortado = archivo.name.split('.')
    let extension = nombreCortado[nombreCortado.length - 1];

    //extensiones permitdas
    let extensionesValidas = ['png', 'jpg', 'gif', 'jpeg']

    if (extensionesValidas.indexOf(extension) < 0) {
        return res.status(400).json({ ok: false, err: { message: 'Las extensiones permitidas son ' + extensionesValidas.join(', ') } })
    }

    //cambiar el nombre al archivo a unico

    let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`


    // console.log(uploadPath);
    archivo.mv(`uploads/${tipo}/${nombreArchivo}`, function (err) {
        if (err) {
            return res.status(500).json({ ok: false, err });
        }
        if (tipo === 'usuario') {
            ImagenUsuario(id, res, nombreArchivo)
        } else {
            ImagenProducto(id, res, nombreArchivo)
        }
    });

})

function ImagenUsuario(id, res, nombreArchivo) {
    Usuario.findById(id, (err, usuarioDB) => {
        if (err) {
            BorrarArchivo(nombreArchivo, 'usuario')
            return res.status(500).json({ ok: false, err });
        }
        if (!usuarioDB) {
            BorrarArchivo(nombreArchivo, 'usuario')
            return res.status(400).json({ ok: false, err: { message: 'Usuario no existe' } });

        }
        BorrarArchivo(usuarioDB.img, 'usuario')

        usuarioDB.img = nombreArchivo;

        usuarioDB.save((err, usuarioGuardado) => {
            res.json({ ok: true, usuario: usuarioGuardado, img: nombreArchivo })
        })



    })
}
function ImagenProducto(id, res, nombreArchivo) {
    Producto.findById(id, (err, productoDB) => {
        if (err) {
            BorrarArchivo(nombreArchivo, 'producto')
            return res.status(500).json({ ok: false, err });
        }
        if (!productoDB) {
            BorrarArchivo(nombreArchivo, 'producto')
            return res.status(400).json({ ok: false, err: { message: 'producto no existe' } });

        }
        BorrarArchivo(productoDB.img, 'producto')

        productoDB.img = nombreArchivo;

        productoDB.save((err, productoGuardado) => {
            res.json({ ok: true, producto: productoGuardado, img: nombreArchivo })
        })



    })
}

function BorrarArchivo(nombreImagen, tipo) {
    let pathUrl = path.resolve(__dirname, `../../uploads/${tipo}/${nombreImagen}`)
    if (fs.existsSync(pathUrl)) {
        fs.unlinkSync(pathUrl)
    }
}
module.exports = app