const express = require('express')
const app = express();
const { verifyToken, verifyAdminRol } = require('../middlewares/autenticacion')
const Categoria = require('../models/categoria')

//Crear categoria

app.post('/categoria', verifyToken, (req, res) => {

    let nombre = req.body.nombre
    let id_user = req.usuario._id
    let categoria = new Categoria({
        nombre,
        usuario: id_user
    })
    categoria.save((err, categoriaDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if (!categoriaDB) {
            return res.status(400).json({
                ok: false,
                err
            })
        }

        res.json({ ok: true, categoria: categoriaDB })
    })

})

//obtener todas las categorias
app.get('/categoria', verifyToken, (req, res) => {
    Categoria.find({})
        .sort('nombre')
        .populate('usuario','nombre email')
        .exec((err, categoriaDB) => {
            if (err) {
                return res.status(500).json({ ok: false, err })
            }
            if (!categoriaDB) {
                return res.status(400).json({ ok: false, err: { message: 'Categoria no encontrada' } })
            }
            res.json({ ok: true, categoria: categoriaDB })
        })
})

//obtener por id

app.get('/categoria/:id', verifyToken, (req, res) => {
    let id_categoria = req.params.id

    Categoria.findById(id_categoria, (err, categoriaDB) => {

        if (err) {
            return res.status(500).json({ ok: false, err: { message: 'Categoria no encontrada' } })
        }
        if (!categoriaDB) {
            return res.status(400).json({ ok: false, err: { message: 'Categoria no encontrada' } })
        }
        res.json({ ok: true, categoria: categoriaDB })
    })

})

//actualizar categoria

app.put('/categoria/:id', verifyToken, (req, res) => {
    let id_categoria = req.params.id
    let nombre = req.body.nombre
    Categoria.findOneAndUpdate(id_categoria, { nombre }, { new: true, runValidators: true }, (err, categoriaDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!categoriaDB) {
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({ ok: true, categoria: categoriaDB });

    })
})

//eliminar la categoria

app.delete('/categoria/:id', [verifyToken, verifyAdminRol], (req, res) => {
    let id = req.params.id

    Categoria.findByIdAndDelete(id, (err, categoriaDB) => {
        if (err) {
            return res.status(500).json({ ok: false, err })
        }
        if (!categoriaDB) {
            return res.status(400).json({ ok: false, err: { message: 'Categoria no encontrada' } })
        }
        res.json({ ok: true, message: 'Categoria borrada' })
    })
})


module.exports = app