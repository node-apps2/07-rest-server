const express = require('express')
const fs = require('fs')
const app = express();
const path = require('path')
const { verifyToken,verifyTokenUrl } = require('../middlewares/autenticacion')

app.get('/imagen/:tipo/:img',verifyTokenUrl, (req, res) => {
    let tipo = req.params.tipo
    let img = req.params.img


    let pathUrl = path.resolve(__dirname, `../../uploads/${tipo}/${img}`)

    if (fs.existsSync(pathUrl)) {
        res.sendFile(pathUrl)

    } else {
        let noImg = path.resolve(__dirname, '../assets/original.jpg')
        res.sendFile(noImg)
    }





})


module.exports = app